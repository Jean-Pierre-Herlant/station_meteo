#pragma once

//Standard Library
#include <functional>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <utility>
#include <thread>
#include <memory>

//containers
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <vector>
#include <tuple>
#include <array>
#include <stack>
#include <set>
#include <map>

//boost 
#include <boost/algorithm/algorithm.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

//other
#include <spdlog/spdlog.h>
#include <fmt/format.h>