#pragma once

#include <pch.hpp>

class serial_port
{
private:
    std::array<char, 50> buffer_;
    boost::asio::serial_port port_;
    boost::asio::io_service io_;
    boost::thread thread_;

public:
    serial_port(std::string const&& name);
    ~serial_port();

    auto read_completed(const boost::system::error_code& error, std::size_t bytes_transferred) -> void;
    auto read_async() -> void;
};

