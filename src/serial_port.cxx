#include "serial_port.hpp"

serial_port::serial_port(std::string const&& name) : port_(io_, name)
{
    using namespace boost;

    port_.set_option(asio::serial_port_base::baud_rate(9600));

    if(!port_.is_open())
    {
        std::cout << "couldn't open port\n";
    }
    this->read_async();
}

serial_port::~serial_port()
{
    port_.close();
}


auto serial_port::read_completed(boost::system::error_code const& error, std::size_t bytes_transferred) -> void
{
    if(error){
        std::cout << "error " << error;
    } else {
        std::cout << "data received" << bytes_transferred << ":\n";
        for(auto const chr : buffer_)
            std::cout << chr;
    }

    this->read_async();
}

auto serial_port::read_async() -> void
{
    port_.async_read_some(boost::asio::buffer(buffer_.data(), buffer_.size()),
                boost::bind(&serial_port::read_completed,
                            this, boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred));

    thread_ = boost::thread(boost::bind(&boost::asio::io_service::run, &io_));
}