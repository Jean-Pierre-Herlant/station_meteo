#include <catch2/catch.hpp>
#include <vector>
#include <bitset>
#include <algorithm>

enum class frame_type { 
	error, 
	wind, 
	rain, 
	thermo_hygro, 
	mushroom, 
	thermo_only, 
	thermo_hygro_baro,
	minute, clock, 
	thermo_hygro_baro2 
};

frame_type check_frame_type(std::vector<unsigned char> frame)
{
	if(frame.size() < 5 or frame.at(2) > 0b0000'1111){
		return frame_type::error;
	}

	switch(frame.at(2)) {
		case 0b0000'0000: 
			return frame_type::wind;
			break;
		case 0b0000'0001:
			return frame_type::rain;
			break;
		case 0b0000'0010:
			return frame_type::thermo_hygro;
			break;
		case 0b0000'0011:
			return frame_type::mushroom;
			break;
		case 0b0000'0100: 
			return frame_type::thermo_only;
			break;
		case 0b0000'0101:
			return frame_type::thermo_hygro_baro;
			break;
		case 0b0000'0110:
			return frame_type::thermo_hygro_baro2;
			break;
		case 0b0000'1110:
			return frame_type::minute;
			break;
		case 0b0000'1111:
			return frame_type::clock;
			break;
		default:
			return frame_type::error;
			break;
	}

}

TEST_CASE("Check the frame type")
{
	std::vector<unsigned char> vec {0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	
	vec.at(2) = 0b0000'0000;
	REQUIRE(check_frame_type(vec) == frame_type::wind);

	vec.at(2) = 0b0000'0001;
	REQUIRE(check_frame_type(vec) == frame_type::rain);

	vec.at(2) = 0b0000'0010;
	REQUIRE(check_frame_type(vec) == frame_type::thermo_hygro);

	vec.at(2) = 0b0000'0011;
	REQUIRE(check_frame_type(vec) == frame_type::mushroom);

	vec.at(2) = 0b0000'0100;
	REQUIRE(check_frame_type(vec) == frame_type::thermo_only);

	vec.at(2) = 0b0000'0101;
	REQUIRE(check_frame_type(vec) == frame_type::thermo_hygro_baro);

	vec.at(2) = 0b0000'1110;
	REQUIRE(check_frame_type(vec) == frame_type::minute);

	vec.at(2) = 0b0000'1111;
	REQUIRE(check_frame_type(vec) == frame_type::clock);

	vec.at(2) = 0b0000'0110;
	REQUIRE(check_frame_type(vec) == frame_type::thermo_hygro_baro2);

	vec.at(2) = 0b0000'0111;
	REQUIRE(check_frame_type(vec) == frame_type::error);

	vec.at(2) = 0b0000'1000;
	REQUIRE(check_frame_type(vec) == frame_type::error);

	vec.at(2) = 0b0000'1001;
	REQUIRE(check_frame_type(vec) == frame_type::error);

	vec.at(2) = 0b000'1010;
	REQUIRE(check_frame_type(vec) == frame_type::error);

	vec.at(2) = 0b0000'1011;
	REQUIRE(check_frame_type(vec) == frame_type::error);

	vec.at(2) = 0b0000'1100;
	REQUIRE(check_frame_type(vec) == frame_type::error);

	vec.at(2) = 0b0000'1101;
	REQUIRE(check_frame_type(vec) == frame_type::error);

	for(unsigned char i = 16; i < 255; ++i)
	{
		vec.at(2) = i;
		REQUIRE(check_frame_type(vec) == frame_type::error);
	}

}


bool parse(std::vector<unsigned char> vec)
{
	if(vec.size() < 5){
		return false;
	}

	if(vec.at(0) != 255 or vec.at(1) != 255){
		return false;
	}

	size_t size {};

	switch(check_frame_type(vec)){
		case frame_type::wind :
			size = 11;
			break;
		case frame_type::rain :
			size = 16;
			break;
		case frame_type::thermo_hygro : 
			size = 9;
			break;
		case frame_type::mushroom : 
			size = 9;
			break;
		case frame_type::thermo_only :
			size = 7;
			break;
		case frame_type::thermo_hygro_baro :
			size = 13;
			break;
		case frame_type::minute :
			size = 5;
			break;
		case frame_type::clock :
			size = 9;
			break;
		case frame_type::thermo_hygro_baro2 :
			size = 14;
			break;
		case frame_type::error : 
			size = 0;
			break;
		default:
			size = 0;
			break;
	}

	return vec.size() == size;
}

TEST_CASE("Check the frame length")
{
	std::vector<unsigned char> vec_wind 				{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	std::vector<unsigned char> vec_rain 				{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	std::vector<unsigned char> vec_thermo_hydro  		{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	std::vector<unsigned char> vec_mushroom 			{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	std::vector<unsigned char> vec_thermo_only 			{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	std::vector<unsigned char> vec_thermo_hygro_baro 	{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	std::vector<unsigned char> vec_minute 				{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	std::vector<unsigned char> vec_clock				{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	std::vector<unsigned char> vec_thermo_hygro_baro2	{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	std::vector<unsigned char> vec_error				{ 0xFF, 0xFF, 0xFF, 0xFF };
	std::vector<unsigned char> vec_error2 				{};

	REQUIRE(parse(vec_wind) == false);
	REQUIRE(parse(vec_error) == false);
	REQUIRE(parse(vec_error2) == false);

	vec_wind.at(2) 					= 0b000'0000;
	vec_rain.at(2) 					= 0b000'0001;
	vec_thermo_hydro.at(2)			= 0b000'0010;
	vec_mushroom.at(2) 				= 0b000'0011;
	vec_thermo_only.at(2) 			= 0b000'0100;
	vec_thermo_hygro_baro.at(2)	 	= 0b000'0101;
	vec_minute.at(2) 				= 0b000'1110;
	vec_clock.at(2) 				= 0b000'1111;
	vec_thermo_hygro_baro2.at(2) 	= 0b000'0110;
	vec_error.at(2)					= 0b000'0000;

	REQUIRE(parse(vec_wind));
	REQUIRE(parse(vec_rain));
	REQUIRE(parse(vec_thermo_hydro));
	REQUIRE(parse(vec_mushroom));
	REQUIRE(parse(vec_thermo_only));
	REQUIRE(parse(vec_thermo_hygro_baro));
	REQUIRE(parse(vec_minute));
	REQUIRE(parse(vec_clock));
	REQUIRE(parse(vec_thermo_hygro_baro2));
	REQUIRE(parse(vec_error) == false);

	for(unsigned char i = 16; i < 255; ++i)
	{
		vec_wind.at(2) = i;
		REQUIRE(check_frame_type(vec_wind) == frame_type::error);
		vec_rain.at(2) = i;
		REQUIRE(check_frame_type(vec_rain) == frame_type::error);
		vec_thermo_hydro.at(2) = i;
		REQUIRE(check_frame_type(vec_thermo_hydro) == frame_type::error);
		vec_mushroom.at(2) = i;
		REQUIRE(check_frame_type(vec_mushroom) == frame_type::error);
		vec_thermo_only.at(2) = i;
		REQUIRE(check_frame_type(vec_thermo_only) == frame_type::error);
		vec_thermo_hygro_baro.at(2) = i;
		REQUIRE(check_frame_type(vec_thermo_hygro_baro) == frame_type::error);
		vec_minute.at(2) = i;
		REQUIRE(check_frame_type(vec_minute) == frame_type::error);
		vec_clock.at(2) = i;
		REQUIRE(check_frame_type(vec_clock) == frame_type::error);
		vec_thermo_hygro_baro2.at(2) = i;
		REQUIRE(check_frame_type(vec_thermo_hygro_baro2) == frame_type::error);
		vec_error.at(2) = i;
		REQUIRE(check_frame_type(vec_error) == frame_type::error);
	}

	vec_wind.at(2) 					= 0b000'0000;
	vec_rain.at(2) 					= 0b000'0001;
	vec_thermo_hydro.at(2)			= 0b000'0010;
	vec_mushroom.at(2) 				= 0b000'0011;
	vec_thermo_only.at(2) 			= 0b000'0100;
	vec_thermo_hygro_baro.at(2)	 	= 0b000'0101;
	vec_minute.at(2) 				= 0b000'1110;
	vec_clock.at(2) 				= 0b000'1111;
	vec_thermo_hygro_baro2.at(2) 	= 0b000'0110;
	vec_error.at(2)					= 0b000'0000;

	vec_wind.push_back(0xFF);
	vec_rain.push_back(0xFF);
	vec_thermo_hydro.push_back(0xFF);
	vec_mushroom.push_back(0xFF);
	vec_thermo_only.push_back(0xFF);
	vec_thermo_hygro_baro.push_back(0xFF);
	vec_minute.push_back(0xFF);
	vec_clock.push_back(0xFF);
	vec_thermo_hygro_baro2.push_back(0xFF);
	
	REQUIRE(parse(vec_wind) == false);
	REQUIRE(parse(vec_rain) == false);
	REQUIRE(parse(vec_thermo_hydro) == false);
	REQUIRE(parse(vec_mushroom) == false);
	REQUIRE(parse(vec_thermo_only) == false);
	REQUIRE(parse(vec_thermo_hygro_baro) == false);
	REQUIRE(parse(vec_minute) == false);
	REQUIRE(parse(vec_clock) == false);
	REQUIRE(parse(vec_thermo_hygro_baro2) == false);
}


bool has_enough_data(std::vector<unsigned char> vec)
{
	if(vec.size() < 7)
	{
		return false;
	}

	auto pos = std::find(vec.begin(), vec.end(), 0b1111'1111);

	while(true)
	{
		if(pos == vec.end() or (pos + 1) == vec.end()){
			return false;
		}
		if(*(pos + 1) != 0b1111'1111)
		{
			pos = std::find(pos + 1, vec.end(), 0b1111'1111);
			continue;
		} else {
			break;
		}
	}

	auto pos2 = std::find(pos + 5, vec.end(), 0b1111'1111);
	while(true)
	{
		if(pos2 == vec.end() or (pos2 + 1) == vec.end()){
			return false;
		}
		if(*(pos2 + 1) != 0b1111'1111)
		{
			pos2 = std::find(pos2 + 1, vec.end(), 0b1111'1111);
			continue;
		} else {
			return true;
		}
	}

}

TEST_CASE("Check if serial has enough data to parse")
{

	std::vector<unsigned char> vec {};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> { 0, 0, 0, 0, 0, 0, 0, 0, 0};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> {0b1111'1111, 0b1111'1111, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> {0b1111'1111, 0b1111'1111, 0b1111'1111, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> { 0b1111'1111, 0b1111'1111, 0, 0, 0, 0, 0, 0, 0, 0};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> { 0, 0, 0, 0, 0, 0, 0, 0, 0b1111'1111, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> { 0b1111'1111, 0, 0, 0, 0, 0, 0, 0, 0, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> { 0, 0, 0, 0, 0, 0, 0, 0, 0b1111'1111, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> { 0b1111'1111, 0b1111'1111, 0, 0, 0, 0, 0, 0, 0, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> { 0, 0, 0b1111'1111, 0, 0b1111'1111, 0, 0, 0, 0, 0, 0, 0b1111'1111, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> { 0, 0, 0b1111'1111, 0b1111'1111, 0, 0, 0, 0, 0, 0, 0b1111'1111, 0, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> { 0, 0, 0b1111'1111, 0, 0b1111'1111, 0, 0, 0, 0, 0, 0, 0b1111'1111, 0, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> { 0b1111'1111, 0, 0b1111'1111, 0, 0, 0, 0, 0, 0, 0b1111'1111, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == false);

	vec = std::vector<unsigned char> { 0b1111'1111, 0b1111'1111, 0, 0, 0, 0, 0, 0, 0b1111'1111, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == true);

	vec = std::vector<unsigned char> { 0b1111'1111, 0b1111'1111, 0, 0, 0, 0, 0, 0, 0b1111'1111, 0b1111'1111, 0, 0, 0};
	REQUIRE(has_enough_data(vec) == true);

	vec = std::vector<unsigned char> { 0, 0, 0b1111'1111, 0b1111'1111, 0, 0, 0, 0, 0, 0, 0b1111'1111, 0b1111'1111};
	REQUIRE(has_enough_data(vec) == true);

	

}